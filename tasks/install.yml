---
- debug:
    msg: "Force re-install EtherCAT Kernel Module? {{ ethercat_force_install }}"

- name: Clean EtherCAT Base Directory
  file:
    path: "{{ ethercat_base_directory }}"
    state: absent
  become: true
  when: (ethercat_force_install)

- name: Create EtherCAT Base Directory
  file:
    path: "{{ ethercat_base_directory }}"
    state: directory
    owner: root
    group: root
    mode: 0777
  become: true

- name: Clone Modbot's Fork of Synapticon's IGH EtherCAT driver repo
  git:
    repo: https://github.com/modbotrobotics/Etherlab_EtherCAT_Master.git
    dest: "{{ ethercat_source_directory }}"
    version: "{{ ethercat_version }}"
  when: (ethercat_force_install) or (dkmsstatus.stdout.find('installed') == -1)

  # Pre-build; ./bootstrap and ./configure source. Script is
  #   copied to source directory and listed in dkms conf.
- name: EtherCAT DKMS Pre-Build Script
  copy:
    src: dkms-prebuild.sh
    dest: "{{ ethercat_source_directory }}/dkms-prebuild.sh"
    owner: root
    group: root
    mode: 0755
  become: true
  when: (ethercat_force_install) or (dkmsstatus.stdout.find('ethercat') == -1)

# Script executed by DKMS to build and install module.
- name: EtherCAT DKMS Configuration
  template:
    src: dkms.conf.j2
    dest: "{{ ethercat_source_directory }}/dkms.conf"
    owner: root
    group: root
    mode: 0755
  become: true
  when: (ethercat_force_install) or (dkmsstatus.stdout.find('ethercat') == -1)

- name: Remove EtherCAT Version from DKMS, if Present
  shell: "dkms remove ethercat/{{ ethercat_version }} --all"
  become: true
  when: (ethercat_force_install) and (dkmsstatus.stdout.find('ethercat') != -1)

- name: Clean DKMS EtherCAT Source Directory
  file:
    path: "/usr/src/ethercat-{{ ethercat_version }}"
    state: absent
  become: true
  when: (ethercat_force_install)

- name: Add EtherCAT to DKMS
  shell: '{{ item }}'
  become: true
  with_items:
    - "cp -R {{ ethercat_source_directory }} /usr/src/ethercat-{{ ethercat_version }}"
    - "dkms add -m ethercat -v {{ ethercat_version }}"
  when: (ethercat_force_install) or (dkmsstatus.stdout.find('ethercat') == -1)

- name: Build EtherCAT Driver
  shell: "dkms build -m ethercat -v {{ ethercat_version }}"
  become: true
  when: (ethercat_force_install) or (dkmsstatus.stdout.find('ethercat') == -1) or (dkmsstatus.stdout.find('added') != -1)

- name: Install EtherCAT driver
  shell: "dkms install -m ethercat -v {{ ethercat_version }} --force"
  become: true
  when: (ethercat_force_install) or (dkmsstatus.stdout.find('ethercat') == -1) or (dkmsstatus.stdout.find('built') != -1)


# EtherCAT Command Line Tool and Wrapper Library
- name: Configure Source for EtherCAT Tool and Wrapper Library
  shell: './dkms-prebuild.sh'
  become: true
  args:
    chdir: "/usr/src/ethercat-{{ ethercat_version }}"
  when: (ethercat_force_install) or (tool_status.stat.islnk is not defined) or (wrap_status.stat.islnk is not defined)

- name: Build EtherCAT Tool and Wrapper Library
  shell: 'make'
  become: true
  args:
    chdir: "/usr/src/ethercat-{{ ethercat_version }}"
  when: (ethercat_force_install) or (tool_status.stat.islnk is not defined) or (wrap_status.stat.islnk is not defined)

- name: Install EtherCAT Tool
  shell: 'cp ethercat /usr/bin'
  become: true
  args:
    chdir: "/usr/src/ethercat-{{ ethercat_version }}/tool"
  when: (ethercat_force_install) or (tool_status.stat.islnk is not defined)

- name: Install EtherCAT Wrapper Library
  shell: 'make install'
  become: true
  args:
    chdir: "/usr/src/ethercat-{{ ethercat_version }}"
  when: (ethercat_force_install) or (wrap_status.stat.islnk is not defined)