# Ansible Ethercat
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [1.1.2] - 2018-08-07

### Changed

- `dkms install` step now uses `--force` argument.


## [1.1.1] - 2018-08-06

### Added

- Check if existing kernel module is present. If module does not exist, or the commandline tool or wrapper library do not exist, proceed with installation tasks.
- Check if kernel module is already installed with DKMS. If installed, uninstall and reinstall (forcing updated version).

### Fixed

- EtherCAT or udev service restart failures no longer fail the role. Useful if not using valid EtherCAT MAC.


## [1.1.0] - 2018-05-22

### Added

- `CHANGELOG.md`.
- `libethercat-wrapper` is built and installed alongside the kernel module.

### Fixed

- `ethercat` systemctl service now starts on system startup, for `multi-user` target (runlevels 2-5).


## [1.0.0]

### Added

- Ansible role installs Modbot Fork of the IGH Ethercat Kernel Module using DKMS, and related systemctl service and master MAC address configuration. Additionally installs ethercat commandline tool.