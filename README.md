Modbot EtherCAT Ansible Role
=========

This Ansible role downloads, builds and installs the EtherCAT kernel module. It also installs the EtherCAT wrapper library and commandline tool.

The EtherCAT kernel module is installed via `DKMS`, so that any number of module versions can be installed on any Linux kernel version that supports it.


The DKMS installation procedure is as follows:

- The EtherCAT source is cloned into a specified source directory (`ethercat_source_directory`), and the specified git branch or tag (`ethercat_version`) is checked out.
- A DKMS pre-build script (`dkms-prebuild.sh`) is copied into the source directory.
- A DKMS configuration file (`dkms.conf`), which details the clean, build, and install steps, is copied into the source directory.
- The EtherCAT source directory is copied to the DKMS source directory (`/usr/src`).
- `dkms add` is called, which adds the source in `/usr/src` to the DKMS build tree.
- `dkms build` is called, which builds the module according to the steps listed in `dkms.conf`. This executes the `PRE_BUILD` step, which `dkms.conf` defines as `dkms-prebuild.sh`.
- `dkms install` is called, which installs the module according to the steps listed in `dkms.conf`.

For more information on DKMS, please consult the [DKMS Documentation](https://help.ubuntu.com/community/DKMS) and [manpage](http://manpages.ubuntu.com/manpages/bionic/man8/dkms.8.html).


This Role has several Task files:

- `main.yml`
  - Polls current kernel version
  - Determines if kernel module exists on the system
- `install-dependencies.yml`
  - Installs Apt-Get dependencies
  - Installs the Linux Kernel headers, if necessary
- `install.yml`
  - Clones the EtherCAT module source and checks out the specified version
  - Copies the DKMS pre-build script and dkms configuration to the ethercat source directory
  - Adds the module to DKMS, after copying the ethercat source directory into the DKMS source directory
  - Builds the module with DKMS
  - Installs the module with DKMS
  - Builds the EtherCAT tool and wrapper library in the original ethercat source directory
  - Installs the EtherCAT tool and wrapper library
- `configure.yml`
  - Templates the EtherCAT service init script
  - Templates the EtherCAT systemd script
  - Templates the EtherCAT configuration file, which specifies any masters' MAC addresses.
  - Starts the EtherCAT service


Installs
------------
Apt Dependencies:  
- ntp
- udev
- mercurial
- autoconf
- libtool
- g++
- cmake
- libncurses5-dev
- dkms
- kernel headers

EtherCAT:  
- EtherCAT via DKMS
- EtherCAT cmdline tool
- ethercat init.d script
- ethercat service
- ethercat sysconfig
- ethercat udev rules

Requirements
------------

N/A


Role Variables
--------------

- kernel: (4.8.15) -- Expected RT-Patched Kernel base version
- rt_patch: (rt10) -- Expected RT-Patched Kernel patch version
- kernel_rt_patch: (`{{ kernel }}-{{ rt_patch }}`) -- Expected RT-Patched Kernel version
- ethercat_base_directory: (`/opt/etherlab`) -- Base directory containing EtherCAT source
- ethercat_source_directory: (`/opt/etherlab/src`) -- Directory in which to clone EtherCAT source
- ethercat_version: (`master`) -- Version (branch or tag) of EtherCAT source to clone
- ethercat_mac_addresses: (`ff:ff:ff:ff:ff:ff`) -- MAC address(es) to add to EtherCAT module config
- ethercat_force_install: (`False`) -- Option to force re-install module


Dependencies
------------

N/A


Example Playbook
----------------

```
- hosts: brains
  gather_facts: yes

  vars:
    kernel: 4.8.15
    rt_patch: rt10
    kernel_rt_patch: "{{ kernel }}-{{ rt_patch }}"
    ethercat_force_install: False
    ethercat_version: master
    ethercat_mac_addresses:
      - aa:bb:cc:dd:ee:ff

  tasks:
    # Install EtherCAT
    - include_role:
        name: modbot.ethercat
```


License
-------

BSD


Author Information
------------------

Modbot Inc. - humphrey@modbot.com
